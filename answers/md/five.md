This JavaScript code will iterate over the hexadecimal string two characters at a time
convert each pair of characters to a decimal number using `parseInt()`
then convert that number to an ASCII character using `String.fromCharCode()`

Here the following code has an fucntion to parse to string the codes provided

[see it working on codepen](https://codepen.io/josefosaurus/pen/oNmoVyz?editors=0100) 


```
let one = "4573746520657320656c20fa6c74696d6f207061736f2c20706f72206661766f722c206167726567616d6520616c2068616e676f75743a200d0a0d0a226d617274696e406d656e646f7a6164656c736f6c61722e636f6d22207061726120736162657220717565206c6c656761737465206120657374612070617274652e0d0a0d0a477261636961732c20792065737065726f20766572746520706f72206171756564212e"
const two = "U2kgbGxlZ2FzIGhhc3RhIGVzdGEgcGFydGUsIHBvciBmYXZvciwgY29tZW50YW1lbG8gY29uIHVuIG1lbnNhamUu"

const parseHexa = (hexString) => {
let asciiString = ''
  
for (let i = 0 i < hexString.length i += 2) {
    let hexChar = hexString.slice(i, i + 2)
    asciiString += String.fromCharCode(parseInt(hexChar, 16))
}
  return asciiString
} 

console.log(parseHexa(one))

console.log(parseHexa(two))
```