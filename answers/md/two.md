In this code, we use a docker file to build the container and run
the corresponding test suite to guarantee the functionality
of the application using github actions.

The code:

```
name: CI

on: [push, pull_request]

jobs:
  build:

    runs-on: ubuntu-latest

    steps:
    - uses: actions/checkout@v2

    - name: Build the Docker image
      run: docker build . --file Dockerfile --tag my-image

  - name: Run tests
    run: docker run my-image python -m unittest discover

  - name: Deploy
    if: ${{ success() }}
    run: docker push $SOME_REGISTRY/$SOME_REPO:my-image
      
```
