# Use an official Nginx runtime as a parent image
FROM nginx:alpine

#copy the folder answers into the container
COPY ./answers /usr/share/nginx/html/answers

#replace the index.html file
COPY ./index.html /usr/share/nginx/html/index.html

# Change the ownership of the /usr/share/nginx/html directory to nginx user
RUN chown -R nginx:nginx /usr/share/nginx/html && \
    chmod -R 755 /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Start Nginx and keep it from running in the background
CMD ["nginx", "-g", "daemon off;"]